package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.logic.colonies.Colony
import theplague.logic.items.Item

class Territory() : ITerritory {

    var player: Player? = null
    var item: Item? = null

    var colony: Colony? = null

    val plagueSize = 0

    override fun iconList(): List<Iconizable> {
        val iconList = mutableListOf<Iconizable>()
        if(item!=null) iconList.add(item!!)
        else if(player!=null) iconList.add(player!!)
        else if(colony!=null) {
            for (i in 1..colony!!.size) iconList.add(colony!!)

        }
        return iconList
    }


}