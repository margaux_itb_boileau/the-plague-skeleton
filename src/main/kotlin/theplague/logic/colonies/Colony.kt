package theplague.logic.colonies

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.items.weapon.Broom
import theplague.logic.items.weapon.Hand
import theplague.logic.items.weapon.Sword
import theplague.logic.items.weapon.Weapon

abstract class Colony :Iconizable {
    abstract override val icon: String
    var size = 1
    //Error en reproduce
    fun willReproduce() = size<3
    fun reproduce() {
        if (willReproduce()) size++
    }
    fun needsToExpand() = size==3
    abstract fun attacked(weapon: Weapon)
    fun colonizedBy(plague:Colony):Colony{
        return plague
    }
    fun expand(position:Position, maxPosition:Position):Colonitzation {
        var newPosition:Position
        do {
            val xRandom = (0 until maxPosition.x).random()
            val yRandom = (0 until maxPosition.y).random()
            newPosition = Position(xRandom,yRandom)
        } while (xRandom!=position.x && yRandom!=position.y)
        return Colonitzation(newPosition, this)
    }

}