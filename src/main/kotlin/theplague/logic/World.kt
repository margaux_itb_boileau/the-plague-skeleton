package theplague.logic


import theplague.interfaces.*
import theplague.logic.colonies.Ant
import theplague.logic.colonies.Colonitzation
import theplague.logic.colonies.Colony
import theplague.logic.colonies.Dragon
import theplague.logic.items.Item
import theplague.logic.items.vehicle.Bicycle
import theplague.logic.items.vehicle.Hellicopter
import theplague.logic.items.vehicle.OnFoot
import theplague.logic.items.vehicle.Vehicle
import theplague.logic.items.weapon.Broom
import theplague.logic.items.weapon.Sword
import theplague.logic.items.weapon.Weapon


class World(override val width:Int, override val height: Int) : IWorld {

    override val territories = List(width) { List(height){Territory()} }
    override val player = Player()
    val currentTerritory get()=  territories[player.position.y][player.position.x]


    init {
       currentTerritory.player=player

    }

    override fun nextTurn() {
        generateNewItems()
        generateNewColonies()
        reproduce()
        //expand()
    }

    private fun generateNewColonies() {
        val position= getPositionRandom()
        if (territories[position.x][position.y].plagueSize!=3) {
            val probabilityColony = (0..100).random()
            val colony =  when(probabilityColony){
                in 0..30-> Ant()
                in 30..40-> Dragon()
                else ->null
            }
            if(colony != null) placeColony(Colonitzation(position,colony))
        }
    }
    private fun placeColony(colonization:Colonitzation) {
        if (currentTerritory.item==null) territories[colonization.position.y][colonization.position.x].colony = colonization.colony
    }
    private fun reproduce() {
        territories.flatten().forEach(){ it.colony?.reproduce() }
    }
    private fun expand() {
        for (i in territories.indices){
            for (j in territories[i].indices){
                if (territories[i][j].colony != null){
                    if (territories[i][j].colony!!.needsToExpand()) {
                        val colonization = territories[i][j].colony!!.expand(Position(i,j), Position(width,height))
                        placeColony(colonization)
                    }
                }
            }
        }
    }
    private fun generateNewItems() {
        val probabilityItem = (0..100).random()
       var item =  when(probabilityItem){
            in 0..25-> Bicycle()
            in 26..35-> Hellicopter()
            in 36..61-> Broom()
            in 62..72-> Sword()
           else ->null

        }
       var position= getPositionRandom()
        if(item !=null) placeItem(item, position)
    }

    private fun placeItem(item: Item, position: Position) {
        territories[position.y][position.x].item=item
    }

    private fun getPositionRandom() :Position{
        val xRandom = (0..width-1).random()
        val yRandom = (0..height-1).random()
        return Position(yRandom,xRandom)
    }
    override fun gameFinished()=false

    override fun canMoveTo(position: Position): Boolean {
        player.currentVehicle.use()
        if (player.currentVehicle.timesLeft==0) player.currentVehicle = OnFoot()
        return player.currentVehicle.canMove(player.position,position)
    }

    override fun moveTo(position: Position) {
        var lastTerritory =currentTerritory
        player.position=position
        currentTerritory.player=player
        lastTerritory.player = null
        println(currentTerritory.iconList())
        exterminate()
    }

    override fun exterminate() {
        if (currentTerritory.colony!=null) currentTerritory.colony!!.attacked(currentTerritory.player!!.currentWeapon)
    }

    override fun takeableItem(): Iconizable? {
         return currentTerritory.item
    }

    override fun takeItem() {
        val item = this.takeableItem()!!
        if (item is Vehicle) {
            player.currentVehicle = item
            currentTerritory.item = null
        }
        if (item is Weapon) {
            player.currentWeapon = item
            currentTerritory.item = null
        }
    }

}