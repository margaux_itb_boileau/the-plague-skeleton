package theplague.logic.items.vehicle

import theplague.interfaces.Position
import theplague.logic.items.vehicle.Vehicle
import kotlin.math.abs

class Bicycle : Vehicle(){
    override val icon: String
        get() = "\uD83D\uDEB2"


    override fun canMove(from: Position, to: Position): Boolean {
        val diferenceX = to.x - from.x
        val diferenceY = to.y - from.y
        return abs(diferenceX+diferenceY) in 0..4
    }
}
