package theplague.logic.items.vehicle

import theplague.interfaces.Position
import theplague.logic.items.vehicle.Vehicle

class Hellicopter() :Vehicle() {
    override val icon: String
        get() = "\uD83D\uDE81"

    override fun canMove(from: Position, to: Position): Boolean {
        return true
    }
}
