package theplague.logic.items.vehicle

import theplague.interfaces.Position

class OnFoot(): Vehicle() {
    override val icon: String = "\uD83D\uDEB6"
    override fun canMove(from: Position, to: Position): Boolean {
        val rang = listOf(-1,1)
        return ((to.x - from.x in rang && to.y - from.y == 0)||
                (to.y - from.y in rang && to.x - from.x == 0))
    }
}