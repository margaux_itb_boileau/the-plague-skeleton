package theplague.logic.items.vehicle

import theplague.logic.items.Item
import theplague.interfaces.*

abstract class Vehicle(): Item() {

    override var timesLeft: Int = 5

    abstract override val icon: String

    override fun use() {
        timesLeft--
    }
    abstract fun canMove(from:Position, to:Position):Boolean
}