package theplague.logic.items

import theplague.interfaces.Iconizable

abstract class Item :Iconizable{
    abstract val timesLeft:Int
    abstract fun use()
}