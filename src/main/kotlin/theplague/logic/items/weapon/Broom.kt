package theplague.logic.items.weapon

import theplague.logic.items.weapon.Weapon

class Broom:Weapon() {
    override val icon: String
        get() = "\uD83E\uDDF9"

}
