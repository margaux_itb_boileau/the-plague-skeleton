package theplague.logic.items.weapon

import theplague.logic.items.Item

abstract class Weapon():Item() {

    override var timesLeft: Int = 5

    abstract override val icon: String

    override fun use() {
        timesLeft--
    }
}