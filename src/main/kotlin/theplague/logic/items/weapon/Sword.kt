package theplague.logic.items.weapon

import theplague.logic.items.weapon.Weapon

class Sword:Weapon() {
    override val icon: String
        get() = "\uD83D\uDDE1"

}
