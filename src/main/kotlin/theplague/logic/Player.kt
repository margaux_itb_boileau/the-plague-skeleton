package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.items.vehicle.OnFoot
import theplague.logic.items.vehicle.Vehicle
import theplague.logic.items.weapon.Hand
import theplague.logic.items.weapon.Weapon

class Player() : IPlayer, Iconizable {

    override val icon = "\uD83D\uDEB6"
    override val turns = 0
    override val livesLeft = 15
    override var currentWeapon:Weapon = Hand()
    override var currentVehicle:Vehicle = OnFoot()
    var position = Position(5, 5)
}